-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Moja_konfetka
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Moja_konfetka
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Moja_konfetka` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `Moja_konfetka` ;

-- -----------------------------------------------------
-- Table `Moja_konfetka`.`agency`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`agency` (
  `id_agency` INT(11) NOT NULL,
  `name_agency` VARCHAR(45) NULL DEFAULT NULL,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `payment_terms` VARCHAR(45) NULL DEFAULT NULL,
  `markup` DECIMAL(15,2) NULL DEFAULT NULL,
  `agency_services_price` DECIMAL(15,2) NULL DEFAULT NULL,
  `agreemen_termination` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_agency`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`personal_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`personal_info` (
  `passport_id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `surname` VARCHAR(45) NULL DEFAULT NULL,
  `age` INT(11) NULL DEFAULT NULL,
  `family_status` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`passport_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`agent`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`agent` (
  `id_agent` INT(11) NOT NULL,
  `personal_info_passport_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_agent`, `personal_info_passport_id`),
  INDEX `fk_agent_personal_info1_idx` (`personal_info_passport_id` ASC) VISIBLE,
  CONSTRAINT `fk_agent_personal_info1`
    FOREIGN KEY (`personal_info_passport_id`)
    REFERENCES `Moja_konfetka`.`personal_info` (`passport_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`agent_has_agency`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`agent_has_agency` (
  `agent_id_agent` INT(11) NOT NULL,
  `agency_id_agency` INT(11) NOT NULL,
  PRIMARY KEY (`agent_id_agent`, `agency_id_agency`),
  INDEX `fk_agent_has_agency_agency1_idx` (`agency_id_agency` ASC) VISIBLE,
  INDEX `fk_agent_has_agency_agent1_idx` (`agent_id_agent` ASC) VISIBLE,
  CONSTRAINT `fk_agent_has_agency_agency1`
    FOREIGN KEY (`agency_id_agency`)
    REFERENCES `Moja_konfetka`.`agency` (`id_agency`),
  CONSTRAINT `fk_agent_has_agency_agent1`
    FOREIGN KEY (`agent_id_agent`)
    REFERENCES `Moja_konfetka`.`agent` (`id_agent`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`landlord`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`landlord` (
  `id_landor` INT(11) NOT NULL,
  `personal_info_passport_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_landor`, `personal_info_passport_id`),
  INDEX `fk_landlord_personal_info1_idx` (`personal_info_passport_id` ASC) VISIBLE,
  CONSTRAINT `fk_landlord_personal_info1`
    FOREIGN KEY (`personal_info_passport_id`)
    REFERENCES `Moja_konfetka`.`personal_info` (`passport_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`agent_has_landlord`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`agent_has_landlord` (
  `agent_id_agent` INT(11) NOT NULL,
  `landlord_id_landor` INT(11) NOT NULL,
  PRIMARY KEY (`agent_id_agent`, `landlord_id_landor`),
  INDEX `fk_agent_has_landlord_landlord1_idx` (`landlord_id_landor` ASC) VISIBLE,
  INDEX `fk_agent_has_landlord_agent1_idx` (`agent_id_agent` ASC) VISIBLE,
  CONSTRAINT `fk_agent_has_landlord_agent1`
    FOREIGN KEY (`agent_id_agent`)
    REFERENCES `Moja_konfetka`.`agent` (`id_agent`),
  CONSTRAINT `fk_agent_has_landlord_landlord1`
    FOREIGN KEY (`landlord_id_landor`)
    REFERENCES `Moja_konfetka`.`landlord` (`id_landor`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`client` (
  `id_client` INT(11) NOT NULL,
  `agent_id_agent` INT(11) NOT NULL,
  `personal_info_passport_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`agent_id_agent`, `personal_info_passport_id`, `id_client`),
  INDEX `fk_client_personal_data_idx` (`id_client` ASC) VISIBLE,
  INDEX `fk_client_personal_info1_idx` (`personal_info_passport_id` ASC) VISIBLE,
  CONSTRAINT `fk_client_agent1`
    FOREIGN KEY (`agent_id_agent`)
    REFERENCES `Moja_konfetka`.`agent` (`id_agent`),
  CONSTRAINT `fk_client_personal_info1`
    FOREIGN KEY (`personal_info_passport_id`)
    REFERENCES `Moja_konfetka`.`personal_info` (`passport_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`regestry_number`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`regestry_number` (
  `regestry_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`regestry_number`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `Moja_konfetka`.`place`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Moja_konfetka`.`place` (
  `id_place` INT(11) NOT NULL,
  `square` INT(11) NULL DEFAULT NULL,
  `price` DECIMAL(15,2) NULL DEFAULT NULL,
  `location` VARCHAR(45) NULL DEFAULT NULL,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  `landlord_id_landor` INT(11) NOT NULL,
  `regestry_number_regestry_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_place`),
  INDEX `fk_place_landlord1_idx` (`landlord_id_landor` ASC) VISIBLE,
  INDEX `fk_place_regestry_number1_idx` (`regestry_number_regestry_number` ASC) VISIBLE,
  CONSTRAINT `fk_place_landlord1`
    FOREIGN KEY (`landlord_id_landor`)
    REFERENCES `Moja_konfetka`.`landlord` (`id_landor`),
  CONSTRAINT `fk_place_regestry_number1`
    FOREIGN KEY (`regestry_number_regestry_number`)
    REFERENCES `Moja_konfetka`.`regestry_number` (`regestry_number`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
