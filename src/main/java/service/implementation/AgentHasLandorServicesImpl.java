package service.implementation;


import dao.implementation.AgentHasLandorDaoImpl;
import model.entitydata.Agent_has_landlord;
import service.inrterfaces.AgentHasLandorServices;

import java.sql.SQLException;
import java.util.List;

public class AgentHasLandorServicesImpl implements AgentHasLandorServices {

    @Override
    public List<Agent_has_landlord> findAll() throws SQLException {
        return new AgentHasLandorDaoImpl().findAll();
    }

    @Override
    public Agent_has_landlord findById(Integer id_agent) throws SQLException {
        return new AgentHasLandorDaoImpl().findById(id_agent);
    }

    @Override
    public int create(Agent_has_landlord agent_has_landlord) throws SQLException {
        return new AgentHasLandorDaoImpl().create(agent_has_landlord);
    }

    @Override
    public int update(Agent_has_landlord agent_has_landlord) throws SQLException {
        return new AgentHasLandorDaoImpl().update(agent_has_landlord);
    }

    @Override
    public int delete(Agent_has_landlord id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_agent) throws SQLException {
        return new AgentHasLandorDaoImpl().delete(id_agent);
    }
}
