package service.implementation;


import dao.implementation.AgencyDaoImpl;
import model.entitydata.Agency;
import service.inrterfaces.AgencyServices;

import java.sql.SQLException;
import java.util.List;

public class AgencyServicesImpl implements AgencyServices {
    @Override
    public List<Agency> findAll() throws SQLException {
        return new AgencyDaoImpl().findAll();
    }

    @Override
    public Agency findById(Integer id_agency) throws SQLException {
        return new AgencyDaoImpl().findById(id_agency);
    }

    @Override
    public int create(Agency agency) throws SQLException {
        return new AgencyDaoImpl().create(agency);
    }

    @Override
    public int update(Agency agency) throws SQLException {
        return new AgencyDaoImpl().update(agency);
    }

    @Override
    public int delete(Agency id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_agency) throws SQLException {
        return new AgencyDaoImpl().delete(id_agency);
    }
}
