package model.meta;

import java.util.ArrayList;
import java.util.List;

public class TableMeta {
    private String DBName;
    private String tableName;
    private List<ColumnMeta> columnMetaData = new ArrayList<>();
    private List<ForeignKeyMeta> foreignKeyList = new ArrayList<>();

    public void setDBName(String DBName) {
        this.DBName = DBName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setColumnMetaData(List<ColumnMeta> columnMetaData) {
        this.columnMetaData = columnMetaData;
    }

    public void setForeignKeyList(List<ForeignKeyMeta> foreignKeyList) {
        this.foreignKeyList = foreignKeyList;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TABLE: " + tableName + "\n");
        for (ColumnMeta column : columnMetaData) {
            stringBuilder.append(column + "\n");
        }
        for (ForeignKeyMeta fk : foreignKeyList) {
            stringBuilder.append(fk + "\n");
        }
        return stringBuilder.toString();
    }

}
