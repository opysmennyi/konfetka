package model.entitydata;

import model.annotation.Column;
import model.annotation.PrimaryKey;
import model.annotation.Table;

@Table(name = "client")
public class Client {
    @PrimaryKey
    @Column(name = "id_client")
    private int id_client;
    @Column(name = "agent_id_agent")
    @PrimaryKey
    private int id_agent;
    @PrimaryKey
    @Column(name = "personal_info_passport_id")
    private String personal_info_passport_id;

    public Client() {
    }

    public Client(int id_client, int id_agent, String personal_info_passport_id) {
        this.id_client = id_client;
        this.id_agent = id_agent;
        this.personal_info_passport_id = personal_info_passport_id;
    }

    public int getId_client() {
        return id_client;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }

    public int getId_agent() {
        return id_agent;
    }

    public void setId_agent(int id_agent) {
        this.id_agent = id_agent;
    }

    public String getPersonal_info_passport_id() {
        return personal_info_passport_id;
    }

    public void setPersonal_info_passport_id(String personal_info_passport_id) {
        this.personal_info_passport_id = personal_info_passport_id;
    }

    @Override
    public String toString() {
        return String.format("%d %d %-11s", id_client, id_agent, personal_info_passport_id);
    }
}
