package view_controller;

import view_controller.finite_methods.AgencyAgentClientLandlordInfo;
import view_controller.finite_methods.AgentClientInfo;
import view_controller.view_submodel.*;

import java.util.LinkedHashMap;
import java.util.Map;

public class MyViewMethodsMenu {
    public Map<String, Printable> methodsMenu;

    public MyViewMethodsMenu() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("A", SelectMethods::selectAllTable);
        methodsMenu.put("B", TakeStructureOfDB::takeStructureOfDB);
        methodsMenu.put("11", CreateMethods::createAgency);
        methodsMenu.put("12", UpdateMethods::updateAgency);
        methodsMenu.put("13", DeleteMethods::deleteAgency);
        methodsMenu.put("14", SelectMethods::selectAgency);
        methodsMenu.put("15", FindByIdMethods::findByIdAgency);
        methodsMenu.put("21", CreateMethods::createAgent);
        methodsMenu.put("22", UpdateMethods::updateAgent);
        methodsMenu.put("23", DeleteMethods::deleteAgent);
        methodsMenu.put("24", SelectMethods::selectAgent);
        methodsMenu.put("25", FindByIdMethods::findByIdAgent);
        methodsMenu.put("31", CreateMethods::createAHA);
        methodsMenu.put("32", UpdateMethods::updateAHA);
        methodsMenu.put("33", DeleteMethods::deleteAHA);
        methodsMenu.put("34", SelectMethods::selectAHA);
        methodsMenu.put("35", FindByIdMethods::findByIdAHA);
        methodsMenu.put("41", CreateMethods::createAHL);
        methodsMenu.put("42", UpdateMethods::updateAHL);
        methodsMenu.put("43", DeleteMethods::deleteAHL);
        methodsMenu.put("44", SelectMethods::selectAHL);
        methodsMenu.put("45", FindByIdMethods::findByIdAHL);
        methodsMenu.put("51", CreateMethods::createClient);
        methodsMenu.put("52", UpdateMethods::updateClient);
        methodsMenu.put("53", DeleteMethods::deleteClient);
        methodsMenu.put("54", SelectMethods::selectClient);
        methodsMenu.put("55", FindByIdMethods::findByIdClient);
        methodsMenu.put("61", CreateMethods::createLandlord);
        methodsMenu.put("62", UpdateMethods::updateLandlord);
        methodsMenu.put("63", DeleteMethods::deleteLandlord);
        methodsMenu.put("64", SelectMethods::selectLandlord);
        methodsMenu.put("65", FindByIdMethods::findByIdLandlord);
        methodsMenu.put("71", CreateMethods::createPersonalInfo);
        methodsMenu.put("72", UpdateMethods::updateePersonalInfo);
        methodsMenu.put("73", DeleteMethods::deletePersonalInfo);
        methodsMenu.put("74", SelectMethods::selectPersonalInfo);
        methodsMenu.put("75", FindByIdMethods::findByIdPersonalInfo);
        methodsMenu.put("81", CreateMethods::createPlace);
        methodsMenu.put("82", UpdateMethods::updatePlace);
        methodsMenu.put("83", DeleteMethods::deletePlace);
        methodsMenu.put("84", SelectMethods::selectPlace);
        methodsMenu.put("85", FindByIdMethods::findByIdPlace);
        methodsMenu.put("91", CreateMethods::createRegestryNumber);
        methodsMenu.put("92", UpdateMethods::updateRegestryNumber);
        methodsMenu.put("93", DeleteMethods::deleteRegestryNumber);
        methodsMenu.put("94", SelectMethods::selectRegestryNumber);
        methodsMenu.put("95", FindByIdMethods::findByIdRegestryNumber);
        methodsMenu.put("W", AgencyAgentClientLandlordInfo::findAgencyAgentClientLandorInfo);
        methodsMenu.put("E", AgentClientInfo::showClientAgentInfo);




    }
}