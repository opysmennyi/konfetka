package view_controller.finite_methods;

import dao.constans_dao.ConstantDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view_controller.constantView.ConstantsView;
import view_controller.view_submodel.CreateMethods;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class AgentClientInfo {
    private static Logger LOG = LogManager.getLogger(CreateMethods.class);

    public static void showClientAgentInfo() throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantsView.INFOCLIENTAGENT)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.info("\nname - " + resultSet.getInt(1) +
                            "\nid gent - " + resultSet.getInt(2) +
                            "\nid client - " + resultSet.getDouble(3));
                }
            }
        }
    }
}
