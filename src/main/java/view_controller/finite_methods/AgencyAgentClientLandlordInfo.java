package view_controller.finite_methods;

import dao.constans_dao.ConstantDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view_controller.constantView.ConstantsView;
import view_controller.view_submodel.CreateMethods;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AgencyAgentClientLandlordInfo {
    private static Logger LOG = LogManager.getLogger(CreateMethods.class);
    public static void findAgencyAgentClientLandorInfo() throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantsView.AACL)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.info("\nid_agency - " + resultSet.getInt(1) +
                            "\nname_agency - " + resultSet.getInt(2) +
                            "\nid_agent - " + resultSet.getDouble(3) +
                            "\nagent passport_id - " + resultSet.getString(4) +
                            "\nid client - " + resultSet.getInt(5) +
                            "\nclient passport_id - " + resultSet.getString(6) +
                            "\nid landor - " + resultSet.getString(7) +
                            "\nlandors passport id - " + resultSet.getString(8));
                }
            }
        }
    }
}
