package view_controller.view_submodel;

import model.meta.TableMeta;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import persistant.persistant_implementation.ConnectionManager;
import service.implementation.MetaService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TakeStructureOfDB {
    private static Logger LOG = LogManager.getLogger(TakeStructureOfDB.class);

    public static void takeStructureOfDB() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        MetaService metaDataService = new MetaService();
        List<TableMeta> tables = metaDataService.getTablesStructure();
        LOG.trace("TABLE OF DATABASE: " + connection.getCatalog());
        for (TableMeta table : tables) {
            LOG.trace(table);
        }
    }
}
