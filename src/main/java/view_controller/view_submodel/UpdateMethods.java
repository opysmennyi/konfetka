package view_controller.view_submodel;

import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;
import view_controller.constantView.ConstantsView;

import java.sql.SQLException;

public class UpdateMethods {
    private static Logger LOG = LogManager.getLogger(UpdateMethods.class);

    public static void updateAgency() throws SQLException {
        LOG.trace("Input agency ID: ");
        Integer id_agency = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("input name for agency: ");
        String name = ConstantsView.INPUT.nextLine();
        LOG.trace("Input city of agency: ");
        String city = ConstantsView.INPUT.nextLine();
        LOG.trace("Input payment terms of agency: ");
        String payment_ters = ConstantsView.INPUT.nextLine();
        LOG.trace("Input agency murk-up: ");
        Integer markup = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input agency's services price: ");
        Integer agency_services_price = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input agreement termination ащк agency: ");
        String agreement_termination = ConstantsView.INPUT.nextLine();
        ConstantsView.INPUT.nextLine();
        Agency agency = new Agency(id_agency, name, city, payment_ters, markup, agency_services_price, agreement_termination);
        AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        int count = agencyServices.update(agency);
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAgent() throws SQLException {
        LOG.trace("Input agent ID: ");
        Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input personal info passport id ");
        String personal_info_passport_id = ConstantsView.INPUT.nextLine();
        Agent agent = new Agent(id_agent, personal_info_passport_id);
        AgentServicesImpl agentServices = new AgentServicesImpl();
        int count = agentServices.update(agent);
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAHA() throws SQLException {
        LOG.trace("Input agent ID: ");
        Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input agency ID: ");
        Integer id_agency = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        Agent_has_agency agent_has_agency = new Agent_has_agency(id_agent, id_agency);
        AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        int count = agentHasAgencyServices.update(agent_has_agency);
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAHL() throws SQLException {
        LOG.trace("Input agent ID: ");
        Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input landlord ID: ");
        Integer id_landlord = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        Agent_has_landlord agent_has_landlord = new Agent_has_landlord(id_agent, id_landlord);
        AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        int count = agentHasLandorServices.update(agent_has_landlord);
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateClient() throws SQLException {
        LOG.trace("Input Client ID: ");
        Integer id_client = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input Agent ID: ");
        Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input personal info(passport id): ");
        String personal_info_passport_id = ConstantsView.INPUT.nextLine();
        Client client = new Client(id_client, id_agent, personal_info_passport_id);
        ClientServicesImpl clientServices = new ClientServicesImpl();
        int count = clientServices.update(client);
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateLandlord() throws SQLException {
        LOG.trace("Input Landlorf ID: ");
        Integer id_landor = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input personal info(passport id): ");
        String personal_info_passport_id = ConstantsView.INPUT.nextLine();
        Landlord landlord = new Landlord(id_landor, personal_info_passport_id);
        LandorServicesImpl landorServices = new LandorServicesImpl();
        int count = landorServices.update(landlord);
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateePersonalInfo() throws SQLException {
        LOG.trace("Input Passport ID: ");
        String  passport_id = ConstantsView.INPUT.nextLine();
        LOG.trace("Input name: ");
        String  name = ConstantsView.INPUT.nextLine();
        LOG.trace("Input surname: ");
        String  surname = ConstantsView.INPUT.nextLine();
        LOG.trace("Input age: ");
        Integer age = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input family status(married/single): ");
        String family_status = ConstantsView.INPUT.nextLine();
        Personal_info personal_info = new Personal_info(passport_id, name, surname, age, family_status);
        PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        int count = personalInfoServices.update(personal_info);
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updatePlace() throws SQLException {
        LOG.trace("Input Place ID: ");
        Integer id_place = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input square for place: ");
        Integer square = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input price for place: ");
        Integer price = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input location of place: ");
        String location = ConstantsView.INPUT.nextLine();
        LOG.trace("Input type of place: ");
        String type = ConstantsView.INPUT.nextLine();
        LOG.trace("Input landlord ID: ");
        Integer landor_id = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input registry number for place: ");
        String regestry_number = ConstantsView.INPUT.nextLine();
        Place place = new Place(id_place, square, price, location, type, landor_id, regestry_number);
        PlaceServicesImpl placeServices = new PlaceServicesImpl();
        int count = placeServices.update(place);
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateRegestryNumber() throws SQLException {
        LOG.trace("Input regestry number: ");
        String regestry_number = ConstantsView.INPUT.nextLine();
        RegestryNumber regestryNumber = new RegestryNumber(regestry_number);
        RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        int count = regestryNumberServices.update(regestryNumber);
        LOG.info(ConstantsView.CREATED + count);
    }
}
