package transformer;

import model.annotation.Column;
import model.annotation.PrimaryKeyComposite;
import model.annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("Duplicates")
public class Transformer<T> {
    private final Class<T> clazz;


    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToAgency(ResultSet resultSet) throws SQLException {
        Object agency = null;
        try {
            agency = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = (Column) field.getAnnotation(Column.class);
                        String name = column.name();
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        if (fieldType == String.class) {
                            field.set(agency, resultSet.getString(name));
                        } else if (fieldType == int.class) {
                            field.set(agency, resultSet.getInt(name));
                        }else if (fieldType == double.class) {
                            field.set(agency, resultSet.getDouble(name));
                        }
                    } else if (field.isAnnotationPresent(PrimaryKeyComposite.class)) {
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        Object FK = fieldType.getConstructor().newInstance();
                        field.set(agency, FK);
                        Field[] fieldsInner = fieldType.getDeclaredFields();
                        for (Field fieldInner : fieldsInner) {
                            if (fieldInner.isAnnotationPresent(Column.class)) {
                                Column column = fieldInner.getAnnotation(Column.class);
                                String name = column.name();
                                fieldInner.setAccessible(true);
                                Class fieldInnerType = fieldInner.getType();
                                if (fieldInnerType == String.class) {
                                    fieldInner.set(FK, resultSet.getString(name));
                                } else if (fieldInnerType == int.class) {
                                    fieldInner.set(FK, resultSet.getInt(name));
                                }else if (fieldType == double.class) {
                                    field.set(agency, resultSet.getDouble(name));
                                }
                            }
                        }
                    }
                }
            }
        } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException e) {
        }

        return agency;
    }
}
