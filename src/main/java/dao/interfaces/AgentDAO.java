package dao.interfaces;

import model.entitydata.Agent;

import java.sql.SQLException;

public interface AgentDAO extends GeneralDAO<Agent, Integer> {
    int delete(Integer id) throws SQLException;
}
