package dao.interfaces;

import model.entitydata.Agency;

import java.sql.SQLException;

public interface AgencyDAO extends GeneralDAO<Agency, Integer> {
    int delete(Integer id) throws SQLException;
}
