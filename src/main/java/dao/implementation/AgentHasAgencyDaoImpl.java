package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.AgentHasAgencyDAO;
import model.entitydata.Agent_has_agency;
import transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AgentHasAgencyDaoImpl implements AgentHasAgencyDAO {
    @Override
    public List<Agent_has_agency> findAll() throws SQLException {
        List<Agent_has_agency> agent_has_agencies = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_AHAD)) {
                while (resultSet.next()) {
                    agent_has_agencies.add((Agent_has_agency) new Transformer(Agent_has_agency.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return agent_has_agencies;
    }

    @Override
    public Agent_has_agency findById(Integer id_agent) throws SQLException {
        Agent_has_agency agent_has_agency = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_AHAD)) {
            preparedStatement.setInt(1, id_agent);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    agent_has_agency = (Agent_has_agency) new Transformer(Agent_has_agency.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return agent_has_agency;
    }

    @Override
    public int create(Agent_has_agency agent_has_agency) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_AHAD)) {
            preparedStatement.setInt(1, agent_has_agency.getId_agent());
            preparedStatement.setInt(2, agent_has_agency.getId_agency());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Agent_has_agency agent_has_agency) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_AHAD)) {
            preparedStatement.setInt(1, agent_has_agency.getId_agent());
            preparedStatement.setInt(2, agent_has_agency.getId_agency());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Agent_has_agency id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_AHAD)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}

