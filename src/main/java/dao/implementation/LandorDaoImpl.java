package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.LandlordDAO;
import model.entitydata.Landlord;
import transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LandorDaoImpl implements LandlordDAO {
    @Override
    public List<Landlord> findAll() throws SQLException {
        List<Landlord> landlords = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_LANDLORD)) {
                while (resultSet.next()) {
                    landlords.add((Landlord) new Transformer(Landlord.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return landlords;
    }

    @Override
    public Landlord findById(Integer id_landlord) throws SQLException {
        Landlord landlord = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_LANDLORD)) {
            preparedStatement.setInt(1, id_landlord);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    landlord = (Landlord) new Transformer(Landlord.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return landlord;
    }

    @Override
    public int create(Landlord landlord) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_LANDLORD)) {
            preparedStatement.setInt(1, landlord.getId_landor());
            preparedStatement.setString(2, landlord.getPersonal_info_passport_id());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Landlord landlord) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_LANDLORD)) {
            preparedStatement.setInt(1, landlord.getId_landor());
            preparedStatement.setString(2, landlord.getPersonal_info_passport_id());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Landlord id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_LANDLORD)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }


}
